//
//  ViewController.m
//  P2
//
//  Created by Click Labs134 on 9/7/15.
//  Copyright (c) 2015 clicklabs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
//@property(nonatomic,strong)UIImageView* img;
@end

@implementation ViewController
UILabel *maths;
UILabel *science;
UILabel *english;
UILabel *hindi;
UILabel *computer;
UITextField *mathstextfield;
UITextField *sciencetextfield;
UITextField *englishtextfield;
UITextField *hinditextfield;
UITextField *computertextfield;
UILabel *percentage;
UILabel *grade;
UIImageView *ImageView;
UILabel *remarks;

- (void)viewDidLoad {
    [super viewDidLoad];
    ImageView= [[ UIImageView alloc] initWithFrame: CGRectMake(120, 70, 100, 100)];
    
    ImageView.backgroundColor= [UIColor blueColor];
    ImageView.image = [UIImage imageNamed: @"rs.jpeg"];
    [self.view addSubview:ImageView];
    
    UILabel *sc= [[UILabel alloc] initWithFrame: CGRectMake(80, 30, 500, 50)];
    sc.text= @"SCORE CARD";
    sc.textColor= [UIColor purpleColor];
    sc.font = [UIFont fontWithName:@"Chalkduster" size:24];
    [self.view addSubview:sc];

    remarks= [[UILabel alloc] initWithFrame: CGRectMake(80, 520, 600, 50)];
    remarks.textColor= [UIColor redColor];
    remarks.font = [UIFont fontWithName:@"Chalkduster" size:22];
    [self.view addSubview:remarks];
    
    UILabel *school= [[UILabel alloc] initWithFrame: CGRectMake(100, 590, 200, 10)];
    school.text= @"www.himanischool.com";
    school.textColor= [UIColor blackColor];
    [self.view addSubview:school];
    
    UILabel *marks= [[UILabel alloc] initWithFrame: CGRectMake(200, 150, 150, 60)];
    marks.text= @"MARKS";
    marks.textColor= [UIColor blueColor];
    marks.font = [UIFont fontWithName:@"Chalkduster" size:18];
    [self.view addSubview:marks];
    
    UILabel *subjects= [[UILabel alloc] initWithFrame: CGRectMake(45, 150, 150, 60)];
    subjects.text= @"SUBJECTS";
    subjects.textColor= [UIColor blueColor];
    subjects.font = [UIFont fontWithName:@"Chalkduster" size:18];
    [self.view addSubview:subjects];
    
    maths= [[UILabel alloc] initWithFrame: CGRectMake(50, 200, 100, 30)];
    maths.text= @"Maths";
    maths.textColor= [UIColor redColor];
    [self.view addSubview:maths];
    
    science= [[UILabel alloc] initWithFrame: CGRectMake(50, 240, 100, 30)];
    science.text= @"Science";
    science.textColor= [UIColor redColor];
    [self.view addSubview:science];
    
    english= [[UILabel alloc] initWithFrame: CGRectMake(50, 280, 100, 30)];
    english.text= @"English";
    english.textColor= [UIColor redColor];
    [self.view addSubview:english];
    
    hindi= [[UILabel alloc] initWithFrame: CGRectMake(50, 320, 100, 30)];
    hindi.text= @"Hindi";
    hindi.textColor= [UIColor redColor];
    [self.view addSubview:hindi];
    
    computer= [[UILabel alloc] initWithFrame: CGRectMake(50, 360, 100, 30)];
    computer.text= @"Computer";
    computer.textColor= [UIColor redColor];
    [self.view addSubview:computer];
    
    mathstextfield= [[UITextField alloc]initWithFrame:CGRectMake(170, 200, 160, 30)];
    mathstextfield.placeholder= @"enter marks";
    mathstextfield.delegate= self;
    mathstextfield.borderStyle= UITextBorderStyleRoundedRect;
    [self.view addSubview:mathstextfield];
    
    sciencetextfield= [[UITextField alloc]initWithFrame:CGRectMake(170, 240, 160, 30)];
    sciencetextfield.placeholder= @"enter marks";
    sciencetextfield.delegate= self;
    sciencetextfield.borderStyle= UITextBorderStyleRoundedRect;
    [self.view addSubview:sciencetextfield];
    
    englishtextfield= [[UITextField alloc]initWithFrame:CGRectMake(170, 280, 160, 30)];
    englishtextfield.placeholder= @"enter marks";
    englishtextfield.delegate= self;
    englishtextfield.borderStyle= UITextBorderStyleRoundedRect;
    [self.view addSubview:englishtextfield];
    
    hinditextfield= [[UITextField alloc]initWithFrame:CGRectMake(170, 320, 160, 30)];
    hinditextfield.placeholder= @"enter marks";
    hinditextfield.delegate= self;
    hinditextfield.borderStyle= UITextBorderStyleRoundedRect;
    [self.view addSubview:hinditextfield];
    
    computertextfield= [[UITextField alloc]initWithFrame:CGRectMake(170, 360, 160, 30)];
    computertextfield.placeholder= @"enter marks";
    computertextfield.delegate= self;
    computertextfield.borderStyle= UITextBorderStyleRoundedRect;
    [self.view addSubview:computertextfield];
    
    UIButton *showpercentage= [[UIButton alloc]initWithFrame:CGRectMake(50, 450, 100, 40)];
    [showpercentage setTitle:@"Show percentage" forState: UIControlStateNormal];
    [showpercentage setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [showpercentage setFont: [UIFont boldSystemFontOfSize:12]];
    [showpercentage addTarget: self action: @selector(showpercentage:) forControlEvents:(UIControlEventTouchUpInside) ];
    [self.view addSubview:showpercentage];
    
    UIButton *showgrade= [[UIButton alloc]initWithFrame:CGRectMake(200, 450, 100, 40)];
    [showgrade setTitle:@"Show grade" forState: UIControlStateNormal];
   [showgrade setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [showgrade setFont: [UIFont boldSystemFontOfSize:12]];
    [showgrade addTarget: self action: @selector (showgrade:) forControlEvents:(UIControlEventTouchUpInside) ];
    [self.view addSubview:showgrade];
    
    percentage= [[UILabel alloc] initWithFrame: CGRectMake(50, 500, 200, 30)];
    percentage.text= @"   percentage";
    percentage.textColor= [UIColor blueColor];
    [ percentage setFont: [UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:percentage];
   
    grade= [[UILabel alloc] initWithFrame: CGRectMake(200, 500, 200, 30)];
    grade.text= @"   grade";
    grade.textColor= [UIColor blueColor];
    [ grade setFont: [UIFont boldSystemFontOfSize:12]];
    [self.view addSubview:grade];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bb.jpeg"]]];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) showpercentage: (UIButton*) sender{
    int a = [mathstextfield.text intValue];
    int b= [sciencetextfield.text intValue];
    int c = [englishtextfield.text intValue];
    int d= [hinditextfield.text intValue];
    int e= [computertextfield.text intValue];
    
    if ([mathstextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Maths field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([sciencetextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Science field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([englishtextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"English field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([hinditextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Hindi field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([computertextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Computer field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([self check: mathstextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self check: sciencetextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self check: englishtextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self check: hinditextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self check: computertextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }

   else if ((a>=0)&&(a<=100)&&(b>=0)&&(b<=100)&&(c>=0)&&(c<=100)&&(d>=0)&&(d<=100)&&(e>=0)&&(e<=100)) {
        int total= (a+b+c+d+e)/5;
        percentage.text = [NSString stringWithFormat: @"your percentage is %d",total];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter valid marks" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
            }
}
-(BOOL) check: (NSString*) text{
    NSCharacterSet *set =[[NSCharacterSet characterSetWithCharactersInString:@"1234567890"]invertedSet];
    if([text rangeOfCharacterFromSet:set].location== NSNotFound )
    {
        NSLog(@"No special character");
        return NO;
    }
    else
    {
        NSLog(@"special character is found");
        return YES;
    }
}
-(void) showgrade: (UIButton*) sender{
    int a = [mathstextfield.text intValue];
    int b= [sciencetextfield.text intValue];
    int c = [englishtextfield.text intValue];
    int d= [hinditextfield.text intValue];
    int e= [computertextfield.text intValue];
    
    if ([mathstextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Maths field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([sciencetextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Science field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([englishtextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"English field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([hinditextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Hindi field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([computertextfield.text length] == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All fields are mandatory" message:@"Computer field is empty" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles:nil];
        [alert show];
    }
    else if ([self check: mathstextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self check: sciencetextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self check: englishtextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self check: hinditextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
    else if ([self check: computertextfield.text ] == YES)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Character is entered" message:@"Enter digits" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }

    
    else if ((a>=0)&&(a<=100)&&(b>=0)&&(b<=100)&&(c>=0)&&(c<=100)&&(d>=0)&&(d<=100)&&(e>=0)&&(e<=100)) {
        int total= (a+b+c+d+e)/5;
        
        grade.text = [NSString stringWithFormat: @"your percentage is :%d",total];
        
        if (total<= 100 && total>=90)
        {
            
            grade.text =  @"your grade is A ";
            ImageView.image = [UIImage imageNamed: @"A.jpeg"];
            remarks.text = @"CONGRATULATIONS!";
        }
        else if (total<=89 && total>=80)
        {
            
            grade.text =  @"your grade is B";
            ImageView.image = [UIImage imageNamed: @"B.jpeg"];
            remarks.text = @"VERY GOOD";
        }
        else if (total<=79 && total>=70)
        {
            
            grade.text = @"your grade is C";
            ImageView.image = [UIImage imageNamed: @"cc.jpeg"];
            remarks.text = @"GOOD";
        }
        
        else if (total<=69  && total>=60)
        {
            
            grade.text = @"your grade is D ";
            ImageView.image = [UIImage imageNamed: @"D.jpeg"];
            remarks.text = @"CAN DO BETTER";
        }
        else if (total<= 59 && total>=1)
        {
            
            grade.text =  @"your grade is E";
            ImageView.image = [UIImage imageNamed: @"E.jpeg"];
            remarks.text = @"NEEDS IMPROVEMENT";
        }
        else if ( total== 0)
        {
            
            grade.text =  @"your grade is F(fail) ";
            ImageView.image = [UIImage imageNamed: @"fail.jpeg"];
            remarks.text = @"WORK HARDER";
        }
        
        
    }
       else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Enter valid marks" delegate:(nil) cancelButtonTitle: @"OK" otherButtonTitles: nil];
        [alert show];
    }
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textfield{
    NSLog(@"textFieldShouldEndEditing");
    textfield.backgroundColor = [UIColor yellowColor];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textfield{
    NSLog(@"textFieldDidEndEditing");
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    textField.backgroundColor = [UIColor greenColor];
    if(textField == mathstextfield)
    {
        maths.textColor= [UIColor purpleColor];
    }
    else if(textField == computertextfield){
    computer.textColor= [UIColor purpleColor];
    }
    else if(textField == sciencetextfield){
    science.textColor= [UIColor purpleColor];
    }
    else if(textField == hinditextfield){
    hindi.textColor= [UIColor purpleColor];
    }
    else if(textField == englishtextfield){
    english.textColor= [UIColor purpleColor];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldDidBeginEditing");
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
